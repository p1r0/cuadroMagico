#!/bin/env python3

class cuadroMagico:
  """
  clase para generar cuadros magicos
  """
  def __init__(self, size):
    self.size = int(size)
    self.valores = []
    self.cuadroM = self.generaCuadro()


  def generaCuadro(self):
    return [[0] * self.size for i in range(self.size)]


  def imprime(self):
    print('\n')
    for i in range(self.size):
      for j in range(self.size):
        print(f"\t{self.cuadroM[i][j]}", end=" ")
      print('\n')
    print('\n')


  def genera(self, start = 1):
    for i in range(start, self.size * self.size):
      self.valores.append(i)


  def resuelve(self, start = 1):
    start = int(start)
    x = 0
    y = int((self.size - 1) / 2)

    self.cuadroM[x][y] = start

    for i in range(start + 1, self.size * self.size + start):
        x_old = x
        y_old = y

        if x == 0:
            x = self.size - 1
        else:
            x -= 1

        if y == self.size - 1:
            y = 0
        else:
            y += 1

        while self.cuadroM[x][y] != 0:
            if x == self.size - 1:
                x = 0
            else:
                x = x_old + 1
            y = y_old

        self.cuadroM[x][y] = i



if __name__ == "__main__":
  size = input("ingresa el tamaño del cuadro magico:")
  inicia = input("ingresa numero para iniciar:")
  cuadroM = cuadroMagico(size)
  cuadroM.imprime()
  # genera desde el parametro numero
  cuadroM.resuelve(inicia)
  cuadroM.imprime()




