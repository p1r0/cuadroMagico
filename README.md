# CuadroMagico

Proyecto para resolver cuadros magicos  por el metodo De La Loubere  y estudiar su comportamiento en un jupyter notebook

## Indice 

[[_TOC_]] 

## Requerimientos
* python >= 3.8.2
* python3-dev == 3.8.2
* python3-setuptools == 45.2.0-1
* python3-pip == 20.0.2
* virtualenv == 20.0.17

## Instalacion

**Primero tienes que tener el sistema base de python3**
```sh
$ sudo apt-get install build-essential python3 python3-dev python3-setuptools python3-pip virtualenv
```
**clona el repositorio y entra a su respectivo directorio:**
```sh
$ git clone https://gitlab.com/p1r0/cuadroMagico.git
$ cd cuadroMagico
```
**Prepara y activa el entorno virtual**
```sh
$ virtualenv -p python3 myenv
$ source myenv/bin/activate
```

**instala requirements.txt en el entorno virtual:**
```sh
(myenv)$ pip3 install -r requirements.txt
```
**Ejecuta el Jupyter Notebook**
```sh 
$ jupyter-notebook
```
> NOTA:
> el servidor debe estar funcionando por default en la siguiente direccion http://127.0.0.1:8888

***
## Licencia

[**UNLICENSE**](./LICENSE).

## Informacion y contacto.

***Desarrolladores***

- David E. Perez Negron [david@neetsec.com](mailto:david@neetsec.com)

## Por Hacer?

 - ???
